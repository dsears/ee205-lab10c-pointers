///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 10c - Pointers - EE 205 - Spr 2022
///
/// Explore pointers for fun and profit
///
/// Usage:  pointers
///
/// Result:
///   Just a little story...
///
/// Example:
///   $ pointers
///
/// Compilation:
///   $ g++ -o pointers -Wall -Wextra pointers.cpp
///   This program will only compile in C++ (with g++) not in C (with gcc)
///
/// @file pointers.cpp
/// @version 1.0
///
/// @see https://c-for-dummies.com/caio/pointer-cheatsheet.php
/// @see https://www.codecademy.com/learn/learn-c-plus-plus/modules/learn-cpp-references-and-pointers/cheatsheet
/// @see https://en.wikipedia.org/wiki/Pointer_(computer_programming)
///
/// @author Dane Sears <dsears@hawaii.edu>
/// @date   29_MAR_2022
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <sstream>

using namespace std;
void firstStory() {
	cout << "The first story" << endl;
	
   char  myChar;
   short myShort;
   int   myInt;
   long  myLong;

	cout << "  Sizeof a char is "  << sizeof( myChar )  << " byte"  << endl;
   cout << "  Sizeof a short is " << sizeof( myShort ) << " bytes" << endl; 
   cout << "  Sizeof an int is " << sizeof( myInt ) << "bytes" << endl; 
   cout << "  Sizeof a long is " << sizeof( myLong ) << "bytes" << endl; 
	cout << endl;

	char*    pMyChar;
   short*   pMyShort;
   int*     pMyInt;
   long*    pMyLong; 

   cout << "  Sizeof a char* is "  << sizeof( pMyChar )  << " bytes" << endl;
   cout << "  Sizeof a short* is " << sizeof( pMyShort ) << " bytes" << endl; 
   cout << "  Sizeof a int* is " << sizeof( pMyInt ) << " bytes" << endl;
   cout << "  Sizeof a long* is " << sizeof( pMyLong ) << " bytes" << endl; 

	cout << endl;
}

void secondStory() {
	cout << "The second story" << endl;

	char  myChar   = 100;
   short myShort  = 200;
   int   myInt    = 300;
   long  myLong   = 400; 

	char*    pMyChar;
   short*   pMyShort; 

	int   *pMyInt;
   long  *pMyLong; 

	pMyChar     = &myChar;
   pMyShort    = &myShort; 
   pMyInt      = &myInt;
   pMyLong     = &myLong; 

	cout << "  The value that pMyChar points to is "  << *pMyChar  << endl;
   cout << "  The value that pMyShort points to is " << *pMyShort << endl;
   cout << "  The value that pMyInt points to is "    << *pMyInt   << endl; 
   cout << "  The value that pMyLong points to is " << *pMyLong << endl;

	cout << endl;

	cout << "  The address of myChar is " << (void*)&myChar << endl;
	cout << "   The value of pMyChar is " << (void*)pMyChar << endl;
cout << endl;
   cout << "  The address of myShort is " << (void*)&myShort << endl;
   cout << "   The value of pMyShort is " << (void*)pMyShort << endl;
cout << endl;
   cout << "  The address of myInt is " << (void*)&myInt << endl;
   cout << "   The value of pMyInt is " << (void*)pMyInt << endl;
cout << endl;
   cout << "  The address of myLong is " << (void*)&myLong << endl;
   cout << "   The value of pMyLong is " << (void*)pMyLong << endl;
   cout << endl;
}

static char*   spMyChar    = NULL ;
static short*  spMyShort   = NULL ; 
static int*    spMyInt     = NULL ; 
static long*   spMyLong    = NULL ; 

void thirdStory() {
	cout << "The third story" << endl;

	if( spMyChar == NULL ) {
		spMyChar = (char*) malloc (sizeof( char ));
		*spMyChar = 100; 
	} else {
		*spMyChar += 5;
	}

   if( spMyShort == NULL ) {
      spMyShort = (short*) malloc (sizeof( short )); 
      *spMyShort = 200;
   } else { 
      *spMyShort += 5;
   }

   if( spMyInt == NULL) { 
      spMyInt = (int*)     malloc (sizeof( int ));
      *spMyInt = 300; 
   } else {
      *spMyInt += 5;
   }

   if( spMyLong == NULL) {
      spMyLong = (long*) malloc (sizeof( long ));
      *spMyLong = 400;
   } else {
      *spMyLong += 5;
   }

	cout << "  The value pointed to by spMyChar is "  << *spMyChar  << endl;
   cout << "  The value pointed to by spMyShort is " << *spMyShort << endl; 
   cout << "  The value pointed to by spMyInt is " << *spMyInt << endl; 
   cout << "  The value pointed to by spMyLong is " << *spMyLong << endl; 
	cout << endl;
}

void fourthStory() {
	cout << "The fourth story" << endl;

	if( spMyChar == NULL ) {
		cout << "  spMyChar is NULL" << endl;
	} else {
		free( spMyChar );
		spMyChar = NULL; 
		cout << "  Released memory under spMyChar" << endl;
	}

   if( spMyShort == NULL) {
      cout << "  spMyShort is NULL" << endl;
   } else {
      free( spMyShort ); 
      spMyShort = NULL; 
      cout << "  Released memory under spMyShort" << endl; 
   }

   if( spMyInt == NULL) {
      cout << "  spMyInt is NULL" << endl;
   } else {
      free( spMyInt );
      spMyInt = NULL; 
      cout << "  Released memory under spMyInt" << endl;
   }

   if( spMyLong == NULL) {
      cout << "  spMyLong is NULL" << endl;
   } else {
      free( spMyLong );
      spMyLong = NULL;
      cout << "  Released memory under spMyLong" << endl;
   }
	cout << endl;
}

void fifthStory() {
	cout << "The fifth story" << endl;
	std::stringbuf myObject;
   std::stringbuf* pMyObject = NULL; 
   pMyObject = &myObject; 
   cout << "  Sizeof a pointer to some object is " << sizeof( pMyObject ) << " bytes" << endl; 
   cout << "  The value of pMyObject is " << (void*)&pMyObject << endl; 
	cout << "  The result of sgetc() is " << (*pMyObject).sgetc() << endl;
   cout << "  The result of in_avail() is " << (*pMyObject).in_avail() << endl; 
	cout << endl;
}

long square( long a ){
   a = a * a ;
   return a;
}


long squareRef( long* a ){ 
   *a = *a * *a;
   return *a;
}

void sixthStory() {
   long a = 4;
   long b = 4;
	cout << "The sixth story" << endl;
   cout << "  The square of " << a << " is << " << square(a) << endl;
   cout << "  The variable a is called by value, so it's still " << a << endl; 
   cout << "  The square of " << b << " is << " << squareRef(&b) << endl;
   cout << "  The variable b is called by reference, so it's now " << b << endl;
	cout << endl;
}

int main() {
	firstStory()  ;
	secondStory() ;
	thirdStory()  ;
	thirdStory()  ;   
	thirdStory()  ;  
	fourthStory() ;
	fourthStory() ; 
	fifthStory()  ;
	sixthStory()  ;
}
